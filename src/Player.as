package  
{
	import org.flixel.*;
	public class Player extends FlxSprite 
	{
		[Embed(source = "../circle.png")] protected var Sprite:Class;
		public function Player() 
		{
			x = FlxG.mouse.x;
			y = FlxG.mouse.y;
			loadGraphic(Sprite, false, false, 64, 64);
		}
		
		private function clicked(player:Player, box:Box):void 
		{
			box.kill();
			Registry.score++;
			
		}
		
		private function gameOver():void 
		{
			//PlayState.timer = 1;
			trace(Registry.score.toString());
			Registry.boxGrp.callAll("kill");
			Registry.roundScore = Registry.score;
			if (Registry.score > Registry.highscore)
			{
				Registry.txtHigh.text = "High Score: " + Registry.score.toString();
				Registry.highscore = Registry.score;
			}
			

			Registry.score = 0;
		}
		
		override public function update():void 
		{
			x = FlxG.mouse.x - 30;
			y = FlxG.mouse.y - 30;
			if (FlxG.mouse.justPressed())
			{
				FlxG.flash(0xFFFFFF, 1);
				FlxG.overlap(this, Registry.boxGrp, clicked);
				gameOver();
			}
			super.update();
		}
		
	}

}