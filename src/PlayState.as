package
{

	import flash.display.ActionScriptVersion;
	import org.flixel.*;

	public class PlayState extends FlxState
	{
		private var txtTime:FlxText;
		private var timer:int = 120;
		override public function create():void
		{
			Registry.boxGrp = new FlxGroup;
			FlxG.mouse.hide();
			for (var i:int = 0; i < 100; i++) 
			{
				add(new Box());
			}
			add(new Player());
			Registry.txtHigh = new FlxText(0, 50, FlxG.width, "High Score: " + Registry.highscore.toString());
			Registry.txtHigh.alignment = "center";
			add(Registry.txtHigh);
			txtTime = new FlxText(0, 100, FlxG.width, "");
			txtTime.alignment = "center";
			txtTime.size = 16;
			add(txtTime);
			Registry.txtGG = new FlxText(0, 75, FlxG.width, "Click to play again!");
			Registry.txtGG.alignment = "center";
			Registry.txtGG.visible = false;
			add(Registry.txtGG);
			Registry.txtScore = new FlxText(0, 25, FlxG.width, "This Round: ");
			Registry.txtScore.alignment = "center";
			Registry.txtScore.visible = false;
			add(Registry.txtScore);
		}
		
		override public function update():void
		{
			timer -= 1;
			if (timer <= 0)
			{
				timer = 0;
				Registry.txtGG.visible = true;
				Registry.txtScore.text = "This Round: " + Registry.roundScore.toString();
				Registry.txtScore.visible = true;
				Registry.boxGrp.callAll("kill");
				if (FlxG.mouse.justPressed())
				{
					FlxG.switchState(new PlayState);
				}
			}
			txtTime.text = timer.toString();
			super.update();
		}
	}
}

