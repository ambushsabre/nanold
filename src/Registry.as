package  
{
	import org.flixel.FlxGroup;
	import org.flixel.FlxText;
	/**
	 * ...
	 * @author Andrew Nissen
	 */
	public class Registry 
	{
		public static var boxGrp:FlxGroup;
		public static var score:int;
		public static var highscore:int;
		public static var roundScore:int;
		
		public static var txtScore:FlxText;
		public static var txtHigh:FlxText;
		public static var txtGG:FlxText;
		public function Registry() 
		{
			
		}
		
	}

}