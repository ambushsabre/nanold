package
{

	import org.flixel.*;

	public class MenuState extends FlxState
	{
		
		private var clicked:Boolean = false;
		private var pressed:Boolean = false;
		[Embed(source = "../burst.mp3")] protected var music:Class;
		override public function create():void
		{
			FlxG.bgColor = 0xff000000;
			
			var goText:FlxText = new FlxText(0, 100, FlxG.width, "You have 2 seconds to get as many squares inside the circle and click! Click now to start!");
			goText.alignment = "center";
			add(goText);
			FlxG.mouse.show();
			FlxG.playMusic(music);
		}
		
		override public function update():void
		{
			if (FlxG.mouse.justPressed())
			{
				FlxG.switchState(new PlayState());
			}
			super.update();	
		}
		
	}
}

