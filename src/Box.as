package  
{
	import org.flixel.*;
	public class Box extends FlxSprite
	{
		[Embed(source = "../box.png")] protected var Sprite:Class;
		public function Box( ) 
		{
			color = FlxU.makeColor(FlxG.random() * 255, FlxG.random() * 255, FlxG.random() * 255);
			x = FlxG.random() * FlxG.width;
			y = FlxG.random() * FlxG.height;
			loadGraphic(Sprite, false, false, 16, 16);
			Registry.boxGrp.add(this);
		}
		
		override public function update():void 
		{
			super.update();
		}
		
	}

}